package com.ys.lab.project.consumer;

import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class YsTopicConsumer {
    @KafkaListener(topics = {"${kafka.default.topic}"}
            , groupId = "${kafka.default.group-id}"
            , concurrency = "1")
    public void ysTopicConsumer(@Payload String message) {
        log.info(">>> message: {}", message);
    }
}
